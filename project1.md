# SQL 
# simple table
CREATE TABLE friends (
  id INTEGER,
  name TEXT,
  birthday DATE
);
INSERT INTO friends (id, name, birthday)
VALUES (1,'Ororo Munroe', '30.05.1940' );

INSERT INTO friends (id,name, birthday)
VALUES (2,'Bob','04.04.2011');

INSERT INTO friends (id,name, birthday)
VALUES (3,'Fred','26.02.1998');

UPDATE friends
SET name = 'Storm'
WHERE id = 1;

ALTER TABLE friends
ADD COLUMN email TEXT;

UPDATE friends 
SET email = 'storm@codecademy.com'
WHERE name = 'Storm';

UPDATE friends 
SET email = 'bob@codecademy.com'
WHERE name = 'Bob';

UPDATE friends 
SET email = 'fred@codecademy.com'
WHERE name = 'Fred';

DELETE FROM friends 
WHERE name = 'Storm';

SELECT * FROM friends;

